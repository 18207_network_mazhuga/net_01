import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;

public class MulticastChecker {
    private int port;
    private InetAddress address;
    private String checkMessage = "check";

    public MulticastChecker(InetAddress address, int port) {
        this.port = port;
        this.address = address;
    }

    public void start() throws IOException {
        byte[] recvBuffer = new byte[1024];
        byte[] sendData = checkMessage.getBytes();

        DatagramPacket sendPackage = new DatagramPacket(sendData, sendData.length, address, port);
        DatagramPacket recvPackage = new DatagramPacket(recvBuffer, recvBuffer.length);

        long lastTime;
        boolean hasChanged;
        ConnectedMap connectedMap = new ConnectedMap();

        try (MulticastSocket socket = new MulticastSocket(port)){
            socket.joinGroup(address);
            socket.setSoTimeout(1000);

            while (true) {
                hasChanged = false;
                socket.send(sendPackage);
                lastTime = System.currentTimeMillis();
                while (System.currentTimeMillis() - lastTime < 3000) {
                    try {
                        socket.receive(recvPackage);

                        if (Utility.isDataEqual(sendPackage, recvPackage)) {
                            //System.out.println("Received data from: " + recvPackage.getAddress().toString());
                            if (!connectedMap.contains(recvPackage.getAddress()))
                                hasChanged = true;
                            connectedMap.setConnected(recvPackage.getAddress());
                        }

                    } catch (SocketTimeoutException e) {
                        //System.out.println("Timeout reached");
                    }
                }

                if (connectedMap.hasDisconnected())
                    hasChanged = true;

                if (hasChanged) {
                    System.out.println("This program is running on following IP addresses: ");

                    for (InetAddress address : connectedMap.getConnected())
                        System.out.println(address);
                    System.out.println();

                    connectedMap.removeDisconnected();
                }

                connectedMap.setAllDisconnected();
            }
        } catch (IOException e) {
            throw e;
        }

    }
}
