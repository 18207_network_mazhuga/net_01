import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) {
        int port = 8080;
        InetAddress address;
        try {
            address = InetAddress.getByName(args[0]);
        } catch (UnknownHostException e) {
            System.out.println("Invalid address");
            return;
        }

        try {
            MulticastChecker multicastChecker = new MulticastChecker(address, port);
            multicastChecker.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}