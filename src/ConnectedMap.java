import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

// хранит активные в данный момент адреса (со значением true)
// и активные в прошлый момент времени (со значением false)

public class ConnectedMap {
    private HashMap<InetAddress, Boolean> map;

    public ConnectedMap() {
        map = new HashMap<>();
    }

    public void setConnected(InetAddress address) {
        map.put(address, true);
    }

    public void setAllDisconnected() {
        for (InetAddress key : map.keySet())
            map.put(key, false);
    }

    public void removeDisconnected() {
        ArrayList<InetAddress> toRemove = new ArrayList<>();

        for (InetAddress key : map.keySet())
            if (!map.get(key))
                toRemove.add(key);

        for (InetAddress key : toRemove)
            map.remove(key);
    }

    public List<InetAddress> getConnected() {
        ArrayList<InetAddress> list = new ArrayList<>();
        for (InetAddress key : map.keySet())
            if (map.get(key))
                list.add(key);
        return list;
    }

    public boolean contains(InetAddress address) {
        return map.containsKey(address);
    }

    public boolean hasDisconnected() {
        for (InetAddress key : map.keySet())
            if (!map.get(key))
                return true;

        return false;
    }

}
