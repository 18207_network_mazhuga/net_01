import java.net.DatagramPacket;

public class Utility {
    public static boolean isDataEqual(DatagramPacket p1, DatagramPacket p2) {
        if (p1.getLength() != p2.getLength())
            return false;

        for (int i = 0; i < p1.getLength(); i++)
            if (p1.getData()[i] != p2.getData()[i])
                return false;

        return true;
    }
}
